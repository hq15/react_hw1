import React from "react";
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";
import modalTemplates from "./components/modal/ModalTemplates.js";


let modalDeclaration = {};
const [modalWindowDeclarations] = Object.values(modalTemplates);
const [buttonFirstModalTemplates, buttonSecondModalTemplates] = modalWindowDeclarations



class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayState: "none",
      classWrapper: "wrapper-empty",
      modalWindowDeclarations: modalWindowDeclarations,
      
    };
  }

  didMount = () => {
    this.setState({
      classWrapper: "wrapper",
    });
  };

  didUnmounted = () => {
    this.setState({
      classWrapper: "wrapper-empty",
    });
  };

  closeModal = (e) => {
    if (e.target.classList.contains("wrapper")) {
      this.setState({
        displayState: "none",
      });
    }
  };

  openModal = (e) => {
    const modalID = e.target.dataset.modalId;
    modalDeclaration = this.state.modalWindowDeclarations.find(
      (item) => item.id === modalID
    );

    this.setState({
      displayState: "open",
    });
  };

  closeModalGeneral = () => {
    this.setState({
      displayState: "none",
    });
  };

  

  render() {



    const {
      className,
      id,
      header,
      closeButton,
      description,
      classNameButton,
      textButtonLeft,
      buttonAction,
      textButtonRight,
    } = modalDeclaration;

    return (
      <>
        <div className={this.state.classWrapper} onClick={this.closeModal}>
       
          <Button
            className={buttonFirstModalTemplates.classNameBtnWindowCall}
            dataModal={buttonFirstModalTemplates.id}
            text={buttonFirstModalTemplates.textBtnWindowCall}
            onClick={this.openModal}
          />
          <Button
            className={buttonSecondModalTemplates.classNameBtnWindowCall}
            dataModal={buttonSecondModalTemplates.id}
            text={buttonSecondModalTemplates.textBtnWindowCall}
            onClick={this.openModal}
          />

          {this.state.displayState === "open" && (
            <Modal
              className={className}
              id={id}
              onClick={this.closeModalGeneral}
              show={this.didMount}
              hidden={this.didUnmounted}
              header={header}
              closeButton={closeButton}
              description={description}
              actions={
                <>
                  <Button
                    className={classNameButton}
                    text={textButtonLeft}
                    onClick={buttonAction}
                  />
                  <Button
                    className={classNameButton}
                    text={textButtonRight}
                    onClick={this.closeModalGeneral}
                  />
                </>
              }
            />
          )}
        </div>
      </>
    );
  }
}

export default App;
