import React from "react";
import "./Button.scss";

class Button extends React.Component {
  render() {
    const { className, dataModal, onClick, text } = this.props;

    return (
      <button className={className} data-modal-id={dataModal} onClick={onClick}>
        {text}
      </button>
    );

  }



    
  }


export default Button;
