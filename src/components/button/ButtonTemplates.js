const buttonFirstModalTemplates = [
  {
    classNameBtnFirst: "btn btn__warning",
    dataModalBtnFirst: "modalID1",
    textBtnFirst: "Open first modal",
  },
];
const buttonSecondModalTemplates = [
  {
    classNameBtnSecond: "btn btn__info",
    dataModalBtnSecond: "modalID2",
    textBtnSecond: "Open second modal",
  },
];

const buttonTemplates = {
  buttonFirstModalTemplates,
  buttonSecondModalTemplates,
};
export default buttonTemplates;
