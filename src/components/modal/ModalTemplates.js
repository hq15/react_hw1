const modalTemplates = [
  {
    id: "modalID1",
    className: "modal modal__warning",
    header: "Do you want to delete this file?",
    description:
      "Lorem res nesciunt ipsam quidem beatae animi molestias illo aperiam possimus a sapiente nam? Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis odit magni obcaecati atque sapiente suscipit modi, architecto, nobis illo cumque nesciunt dolores voluptatem, sint labore! Distinctio delectus quibusdam soluta est!",
    classNameButton: "modal__btn",
    textButtonLeft: "Ok",
    textButtonRight: "Cancel",
    closeButton: true,
    buttonAction() {
      alert("The file has been deleted");
    },
    classNameBtnWindowCall: "btn btn__warning",
    //dataModalBtn: "modalID1",
    textBtnWindowCall: "Open first modal",
    
  },
  {
    id: "modalID2",
    className: "modal modal__info",
    header: "Do you want to download this file?",
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloremque facere nulla officiis fuga facilis? Quisquam labore accusamus deserunt temporibus, esse, consequuntur sequi velit non mollitia debitis alias, ullam voluptatem culpa.",
    classNameButton: "modal__btn",
    textButtonLeft: "Download",
    textButtonRight: "Cancel",
    closeButton: false,
    buttonAction() {
      console.log("This file has been uploaded");
    },
    classNameBtnWindowCall: "btn btn__info",
    //dataModalBtn: "modalID2",
    textBtnWindowCall: "Open second modal",
  },
];

// eslint-disable-next-line
export default { modalTemplates };
