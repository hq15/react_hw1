import React from "react";
import "./Modal.scss";

class Modal extends React.Component {
  componentDidMount() {
    this.props.show();
  }

  componentWillUnmount() {
    this.props.hidden();
  }

  render() {
    const { className, header, closeButton, onClick, description, actions } =
      this.props;

    return (
      <>
        <div className={className}>
          <div className="modal__header">
            <h3>{header}</h3>
            {closeButton && (
              <button className="btn__close" onClick={onClick}>
                X
              </button>
            )}
          </div>
          <p className="modal__text">{description}</p>
          {actions}
        </div>
      </>
    );
  }
}

export default Modal;
